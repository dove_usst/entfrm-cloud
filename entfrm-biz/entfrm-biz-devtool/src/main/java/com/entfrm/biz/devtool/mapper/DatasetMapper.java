package com.entfrm.biz.devtool.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.devtool.entity.Dataset;

/**
 * @author entfrm
 * @date 2020-06-12 21:56:29
 *
 * @description 数据源Mapper接口
 */
public interface DatasetMapper extends BaseMapper<Dataset>{

}
