# entfrm-cloud

#### 介绍
entfrm快速开发平台，基于springboot + springcloud + spring-cloud-alibaba的微服务架构

#### 技术选型
* springboot
* spring-security-oauth2
* springcloud-gateway
* nacos
* sentinel
* ribbon
