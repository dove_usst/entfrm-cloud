package com.entfrm.core.base.constant;

/**
 * @author entfrm
 * @date 2020/3/19
 * @description security
 */
public interface SecurityConstants {
    /**
     *  授权token url
     */
    String AUTH_TOKEN = "/oauth/token";
    /**
     *  注销token url
     */
    String TOKEN_LOGOUT = "/token/logout";
    /**
     * BASIC认证
     */
    String HEADER_BASIC = "Basic ";
    /**
     * token请求头名称
     */
    String TOKEN_HEADER = "Authorization";



}
